# Todo list

A simple todo list application

## Prerequisites

- [Node.js > 14](https://nodejs.org) and npm (Recommended: Use [nvm](https://github.com/nvm-sh/nvm))
- [Xcode 12](https://developer.apple.com/xcode)
- [JDK > 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Android Studio and Android SDK](https://developer.android.com/studio)

## Base dependencies

- [react-navigation](https://reactnavigation.org/) navigation library.
- [redux-toolkit](https://github.com/reduxjs/redux-toolkit) for state management.
- [rtk-query](https://github.com/reduxjs/redux-toolkit/blob/master/docs/rtk-query/overview.md) for data fetching and caching.
- [styled-components](https://github.com/styled-components/styled-components) for styling.
