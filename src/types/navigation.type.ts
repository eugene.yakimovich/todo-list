import { NativeStackScreenProps } from '@react-navigation/native-stack';

export type RootStackParamList = {
  Home: undefined;
  Todo: { id: number; title: string };
};

export type HomeScreenProps = NativeStackScreenProps<
  RootStackParamList,
  'Home'
>;

export type TodoScreenProps = NativeStackScreenProps<
  RootStackParamList,
  'Todo'
>;
