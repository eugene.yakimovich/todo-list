import { createApi } from '@reduxjs/toolkit/query/react';

import { TodosType } from '../../types';
import { BASE_URL } from '../../constants';
import { ReduxUtils } from '../../utils';

export const todosAPI = createApi({
  reducerPath: 'todosAPI',
  baseQuery: ReduxUtils.fetchBaseQueryUtil(BASE_URL),
  endpoints: (builder) => ({
    getTodos: builder.query<TodosType, number>({
      query: (limit) => ({
        url: '/todos',
        params: { limit },
        method: 'GET',
      }),
    }),
  }),
});

export const { useGetTodosQuery } = todosAPI;
