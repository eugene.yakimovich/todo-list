import { createSelector } from '@reduxjs/toolkit';

import { ApiSelectorsUtils } from '../../../utils/apiSelectors.utils';
import { TodoType, TodosType } from '../../../types';

type TodosDataSelector = {
  todos: TodosType;
};

const defaultData: TodoType[] = [];

const dataSelector = ApiSelectorsUtils.createDataSelector(defaultData);

const todosSelector = createSelector(
  dataSelector,
  (data: TodosDataSelector) => {
    return data?.todos || defaultData;
  },
);

export const TodoSelectors = {
  todosSelector,
};
