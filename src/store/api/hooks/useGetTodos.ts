import { useMemo } from 'react';

import { useGetTodosQuery } from '../todosAPI';
import { TodosType } from '../../../types';
import { TodoSelectors } from '../selectors';
import { UseQueryHookResult } from '@reduxjs/toolkit/dist/query/react/buildHooks';

type DataResult = {
  todos: TodosType;
  isTodosSuccess: boolean;
  isTodosLoading: boolean;
  refetch: () => void;
};

const selectFromResult = (result: UseQueryHookResult<any, any>): DataResult => {
  const dataResult: DataResult = {
    todos: [],
    isTodosSuccess: result.isSuccess,
    isTodosLoading: result.isLoading,
    refetch: result.refetch,
  };

  dataResult.todos = TodoSelectors.todosSelector(result);

  return dataResult;
};

const useGetTodos = (limit: number = 0, options: any = {}): DataResult => {
  const result: UseQueryHookResult<any, any> = useGetTodosQuery(limit, options);

  const data: DataResult = useMemo(() => {
    return selectFromResult(result);
  }, [result]);

  return data;
};

export default useGetTodos;
