import { AnyAction, Middleware } from '@reduxjs/toolkit';

export interface TodosAPI {
  reducerPath: string;
  reducer: (state: any, action: AnyAction) => any;
  middleware: Middleware;
}

export interface Reducer {
  [reducerPath: string]: (state: any, action: AnyAction) => any;
}
