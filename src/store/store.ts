import {
  configureStore,
  combineReducers,
  PreloadedState,
  Middleware,
} from '@reduxjs/toolkit';

import { todosAPI } from './api/todosAPI';
import { todoReducer } from './slices/todoSlice';
import { searchReducer } from './slices/searchSlice';
import { Reducer, TodosAPI } from './store.types';

const APIS: TodosAPI[] = [todosAPI];

const reducers: Reducer = APIS.reduce(
  (acc, { reducerPath, reducer }): Reducer => {
    return {
      ...acc,
      [reducerPath]: reducer,
    };
  },
  {},
);

const middlewares: Middleware[] = APIS.map(
  ({ middleware }): Middleware => middleware,
);

const rootReducer = combineReducers({
  todos: todoReducer,
  search: searchReducer,
  ...reducers,
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(middlewares),
    preloadedState,
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
