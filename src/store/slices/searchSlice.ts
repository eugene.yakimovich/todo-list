import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface SearchValue {
  searchValue: string;
}

const initialState: SearchValue = {
  searchValue: '',
};

const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    setSearchValue: (state, action: PayloadAction<string>) => {
      state.searchValue = action.payload;
    },
    resetState: (state) => {
      state.searchValue = initialState.searchValue;
    },
  },
});

export const searchActions = searchSlice.actions;
export const searchReducer = searchSlice.reducer;

export default searchSlice.reducer;
