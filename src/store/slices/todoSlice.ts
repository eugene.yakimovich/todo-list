import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { TodoType, TodosType } from '../../types';

export interface TodoSliceState {
  items: TodosType;
}

const initialState: TodoSliceState = {
  items: [{ id: 0, todo: 'Todo from store', userId: 10, completed: false }],
};

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<TodoType>) => {
      const todo: TodoType = {
        id: Math.random(),
        todo: action.payload.todo,
        userId: action.payload.userId,
        completed: action.payload.completed,
      };

      state.items.unshift(todo);
    },
    deleteTodo: (state, action: PayloadAction<number>) => {
      state.items = state.items.filter(
        (todo: TodoType) => todo.id !== action.payload,
      );
    },
    resetState: (state) => {
      state.items = [];
    },
  },
});

export const todoActions = todoSlice.actions;
export const todoReducer = todoSlice.reducer;

export default todoSlice.reducer;
