import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const featureSelector = (state: RootState) => state.search.searchValue;

const searchSelector = createSelector(
  featureSelector,
  (search: string): string => {
    return search;
  },
);

export const SearchSelectors = {
  searchSelector,
};
