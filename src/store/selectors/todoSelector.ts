import { createSelector } from '@reduxjs/toolkit';

import { RootState } from '../store';
import { TodosType } from '../../types';
import { TodoSliceState } from '../slices/todoSlice';

const featureSelector = (state: RootState) => state.todos;

const todosSelector = createSelector(
  featureSelector,
  (todos: TodoSliceState): TodosType => {
    return todos.items;
  },
);

export const TodosSelectors = {
  todosSelector,
};
