export const COLORS = {
  GREY: '#cccccc',
  BLACK: '#021211',
  WHITE: '#ffffff',
  RED: '##ff0000',
  BLUE: '#0064ff',
  GREEN: '#00ff35',
};
