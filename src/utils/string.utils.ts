export const truncateTodoName = (
  str: string,
  charactersQuantity: number,
): string => {
  if (str.length >= charactersQuantity) {
    return str.substring(0, charactersQuantity) + '...';
  }

  return str;
};
