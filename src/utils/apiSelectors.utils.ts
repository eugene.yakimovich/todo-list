type DefaultData = any[];
type ResultData = { data?: any };

const createDataSelector =
  (defaultValue: DefaultData) => (result: ResultData) => {
    return result?.data || defaultValue;
  };

export const ApiSelectorsUtils = {
  createDataSelector,
};
