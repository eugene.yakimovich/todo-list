import { fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const fetchBaseQueryUtil = (baseUrl: string) => {
  return fetchBaseQuery({ baseUrl });
};

export const ReduxUtils = {
  fetchBaseQueryUtil,
};
