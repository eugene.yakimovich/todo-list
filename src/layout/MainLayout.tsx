import React, { memo } from 'react';
import { LayoutContainer, MainLayoutProps } from '../layout';

const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  return <LayoutContainer>{children}</LayoutContainer>;
};

export default memo(MainLayout);
