import React, { memo } from 'react';

import { Container, Text } from '../NoRecords';

const NoRecords = () => {
  return (
    <Container>
      <Text>No records</Text>
    </Container>
  );
};

export default memo(NoRecords);
