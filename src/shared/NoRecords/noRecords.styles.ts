import { styled } from 'styled-components/native';

export const Container = styled.View`
  height: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: 400;
`;
