import React, { memo } from 'react';
import { ViewStyle } from 'react-native';
import styled from 'styled-components/native';

import { COLORS } from '../../constants';

type ButtonContainerType = {
  $disabled: boolean;
};

const ButtonContainer = styled.TouchableOpacity<ButtonContainerType>`
  min-width: 100px;
  border-radius: 8px;
  padding: 12px;
  background: ${COLORS.BLACK};
  opacity: ${({ $disabled }) => ($disabled ? 0.4 : 1)};
`;

const ButtonText = styled.Text`
  font-size: 16px;
  color: ${COLORS.WHITE};
  font-weight: bold;
  align-self: center;
`;

interface ButtonPropsType {
  children: React.ReactNode;
  onPress: () => void;
  customStyles?: ViewStyle;
  disabled?: boolean;
}

const Button: React.FC<ButtonPropsType> = ({
  children,
  disabled,
  onPress,
  customStyles,
}) => {
  return (
    <ButtonContainer
      style={customStyles}
      onPress={onPress}
      disabled={disabled}
      $disabled={disabled as boolean}
    >
      <ButtonText>{children}</ButtonText>
    </ButtonContainer>
  );
};

export default memo(Button);
