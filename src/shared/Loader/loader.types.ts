export type LoaderSize = 'small' | 'large';

export interface LoaderProps {
  size?: LoaderSize;
  color?: string;
}
