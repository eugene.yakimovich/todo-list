import React, { memo } from 'react';
import { ActivityIndicator } from 'react-native';

import { COLORS } from '../../constants';
import { LoaderProps } from '../Loader';

const Loader: React.FC<LoaderProps> = ({
  color = COLORS.BLACK,
  size = 'small',
}) => {
  return <ActivityIndicator color={color} size={size} />;
};

export default memo(Loader);
