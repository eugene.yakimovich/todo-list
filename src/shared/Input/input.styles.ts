import { styled } from 'styled-components/native';

import { InputFieldType, InputWrapperType } from '../Input';
import { COLORS } from '../../constants';

export const InputWrapper = styled.View<InputWrapperType>`
  flex-direction: row;
  align-items: center;
  max-width: ${({ $maxWidth }) => $maxWidth || '250px'};
  margin-bottom: ${({ $marginBottom }) => $marginBottom || '0px'};
`;

export const InputLabel = styled.Text`
  margin-right: 10px;
  font-size: 16px;
  font-weight: 500;
`;

export const InputContainer = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  border-width: 1px;
  border-color: ${COLORS.GREY};
  border-radius: 8px;
  padding: 10px 12px;
`;

export const InputField = styled.TextInput<InputFieldType>`
  width: 100%;
  max-width: ${({ $maxWidth }) => $maxWidth || '200px'};
`;

export const Prefix = styled.View`
  margin-right: 8px;
`;

export const Suffix = styled.View`
  margin-left: 8px;
`;
