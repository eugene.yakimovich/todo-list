import React, { memo } from 'react';

import {
  InputContainer,
  InputField,
  InputLabel,
  InputProps,
  InputWrapper,
  Prefix,
  Suffix,
} from '../Input';

const Input: React.FC<InputProps> = ({
  name,
  label,
  prefix,
  suffix,
  customStyles,
  ...props
}) => {
  const { inputWrapper, inputField } = customStyles!;

  return (
    <InputWrapper
      $marginBottom={inputWrapper.$marginBottom}
      $maxWidth={inputWrapper.$maxWidth}
    >
      {label && <InputLabel>{label}</InputLabel>}

      <InputContainer>
        {prefix && <Prefix>{prefix}</Prefix>}

        <InputField id={name} {...props} $maxWidth={inputField.$maxWidth} />

        {suffix && <Suffix>{suffix}</Suffix>}
      </InputContainer>
    </InputWrapper>
  );
};

export default memo(Input);
