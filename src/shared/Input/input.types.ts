import { ReactNode } from 'react';
import { TextInputProps } from 'react-native';

export type InputWrapperType = {
  $maxWidth?: number;
  $marginBottom?: number;
};

export type InputFieldType = {
  $maxWidth?: number;
};

export type CustomInputStyles = {
  inputWrapper: InputWrapperType;
  inputField: InputFieldType;
};

export interface InputProps extends TextInputProps {
  name: string;
  label?: string;
  prefix?: ReactNode;
  suffix?: ReactNode;
  customStyles?: CustomInputStyles;
}
