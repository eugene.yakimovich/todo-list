import { useState, useEffect } from 'react';

const useDebounce = <T>(value: T, delay: number) => {
  const [debauncedValue, setDebauncedValue] = useState<T>(value);

  useEffect(() => {
    const handler: NodeJS.Timeout = setTimeout(() => {
      setDebauncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debauncedValue;
};

export default useDebounce;
