export const MAIN_ROUTES = {
  ROOT: '/',
  HOME: '/home',
  TODOS: '/todos',
};

export const TODO_ROUTES = {
  TODO: MAIN_ROUTES.TODOS + '/:todoId',
};

export const ROUTE_NAMES = {
  [TODO_ROUTES.TODO]: 'Todo item',
};

export const ROUTE_SHORT_NAMES = {
  [TODO_ROUTES.TODO]: 'Todo',
};

export const NAVIGATION_TITLES = {
  TODO_LIST: 'Todo list',
  TODO_ITEM: 'Todo item',
};
