import React, { memo } from 'react';

import { AddTodoItem, Search, TodoList } from '../../components';
import { HomeScreenProps } from '../../types';
import { MainLayout } from '../../layout';

const HomeScreen: React.FC<HomeScreenProps> = ({ navigation, route }) => {
  return (
    <MainLayout>
      <Search />

      <AddTodoItem />

      <TodoList navigation={navigation} route={route} />
    </MainLayout>
  );
};

export default memo(HomeScreen);
