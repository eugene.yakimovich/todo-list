import { useMemo } from 'react';

import useGetTodos from '../../../store/api/hooks/useGetTodos';
import { TodoType } from '../../../types';
import useFilteredTodoList from './useFilteredTodoList';
import { useTypedSelector } from '../../../hooks';
import { SearchSelectors, TodosSelectors } from '../../../store/selectors';

type ParamsType = {
  limit?: number;
};

const useTodoList = ({ limit = 0 }: ParamsType) => {
  const items = useTypedSelector(TodosSelectors.todosSelector);
  const searchValue = useTypedSelector(SearchSelectors.searchSelector);

  const { todos, isTodosLoading, isTodosSuccess, refetch } = useGetTodos(limit);

  const todoList = useMemo(
    () =>
      [...items, ...todos].map((todo: TodoType) => ({
        id: todo.id,
        todo: todo.todo,
        completed: todo.completed,
        userId: todo.userId,
      })),
    [items, todos],
  );

  const filteredTodoList = useFilteredTodoList(searchValue, todoList);

  const isLoaded = !isTodosLoading && isTodosSuccess;

  return {
    todoList: filteredTodoList,
    isLoaded,
    isTodosLoading,
    isTodosSuccess,
    refetch,
  };
};

export default useTodoList;
