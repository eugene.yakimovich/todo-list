import { useMemo } from 'react';

import { TodoType, TodosType } from '../../../types';
import useDebounce from '../../../hooks/useDebounce';
import { DELAY_VALUE } from '../../../constants';

const useFilteredTodoList = (
  searchValue: string,
  todoList: TodosType,
): TodosType => {
  const debauncedValue = useDebounce(searchValue, DELAY_VALUE);

  const filteredTodoList = useMemo(
    () =>
      todoList.filter(({ todo }: TodoType): string | boolean => {
        if (debauncedValue === '') {
          return todo;
        }

        const isFullNameExists = todo
          .toLowerCase()
          .includes(debauncedValue.toLowerCase());

        return isFullNameExists;
      }),
    [todoList, debauncedValue],
  );

  return filteredTodoList;
};

export default useFilteredTodoList;
