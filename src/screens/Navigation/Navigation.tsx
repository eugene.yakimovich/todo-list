import React, { memo } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { HomeScreen, TodoScreen } from '../../screens';
import { NAVIGATION_TITLES } from '../../routes.constants';
import { RootStackParamList } from '../../types';

const Stack = createNativeStackNavigator<RootStackParamList>();

const Navigation: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: NAVIGATION_TITLES.TODO_LIST }}
        />

        <Stack.Screen
          name="Todo"
          component={TodoScreen}
          options={{ title: NAVIGATION_TITLES.TODO_ITEM }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default memo(Navigation);
