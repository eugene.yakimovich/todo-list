import React, { memo } from 'react';
import { TodoScreenProps } from '../../types';
import { MainLayout } from '../../layout';

import { TextItem } from '../TodoScreen';

const TodoScreen: React.FC<TodoScreenProps> = ({ route }) => {
  const { id, title } = route.params;

  return (
    <MainLayout>
      <TextItem>id: {id}</TextItem>

      <TextItem>Title: {title}</TextItem>
    </MainLayout>
  );
};

export default memo(TodoScreen);
