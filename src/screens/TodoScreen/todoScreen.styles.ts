import { styled } from 'styled-components/native';

export const TextItem = styled.Text`
  text-align: center;
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 10px;
`;
