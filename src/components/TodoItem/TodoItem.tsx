import React, { memo, useState } from 'react';
import { Alert, GestureResponderEvent, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { TodoType } from '../../types';
import { truncateTodoName } from '../../utils';
import { IconsContainer, TodoItemContainer, TodoItemTitle } from '../TodoItem';

interface TodoItemProps extends TodoType {
  onDelete: (id: number) => void;
}

const TodoItem: React.FC<TodoItemProps> = ({
  todo,
  completed,
  id,
  onDelete,
}) => {
  const [isTodoCompleted, setIsTodoCompleted] = useState<boolean>(completed);

  const handleToggleTodoState = (event: GestureResponderEvent) => {
    event.stopPropagation();

    setIsTodoCompleted((prevState) => !prevState);
  };

  const handleDeleteTodo = (event: GestureResponderEvent) => {
    event.stopPropagation();

    Alert.alert(
      'Delete todo',
      'Are you sure you want to delete the record?',
      [
        {
          text: 'Ask me later',
          onPress: () => ({}),
        },
        {
          text: 'Cancel',
          onPress: () => ({}),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => onDelete(id) },
      ],
      { cancelable: true },
    );
  };

  return (
    <TodoItemContainer>
      <TodoItemTitle $isTodoCompleted={isTodoCompleted}>
        {truncateTodoName(todo, 35)}
      </TodoItemTitle>

      <IconsContainer>
        <TouchableOpacity onPress={handleToggleTodoState}>
          {isTodoCompleted ? (
            <FontAwesome name="remove" size={20} color="red" />
          ) : (
            <FontAwesome name="check" size={20} color="lightgreen" />
          )}
        </TouchableOpacity>

        <TouchableOpacity onPress={handleDeleteTodo}>
          <FontAwesome name="trash" size={20} color="red" />
        </TouchableOpacity>
      </IconsContainer>
    </TodoItemContainer>
  );
};

export default memo(TodoItem);
