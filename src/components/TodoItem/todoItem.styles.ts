import { styled } from 'styled-components/native';

import { TodoItemTitleType } from './todoItem.types';

export const TodoItemContainer = styled.View`
  flex-direction: row;
  align-items: baseline;
  justify-content: space-between;
  padding: 15px 0;
  border-bottom-width: 1px;
  border-bottom-color: rgba(0, 0, 0, 0.1);
  border-bottom-style: solid;
`;

export const IconsContainer = styled.View`
  flex-direction: row;
  align-items: center;
  gap: 10px;
`;

export const TodoItemTitle = styled.Text<TodoItemTitleType>`
  font-size: 17px;
  font-weight: 700;
  text-decoration: ${({ $isTodoCompleted }) =>
    $isTodoCompleted ? 'line-through' : 'none'};
`;
