export type TodoItemTitleType = {
  $isTodoCompleted: boolean;
};
