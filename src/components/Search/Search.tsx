import React, { memo, useState } from 'react';
import SearchIcon from 'react-native-vector-icons/FontAwesome';
import CrossIcon from 'react-native-vector-icons/FontAwesome';

import { Input } from '../../shared';
import { searchActions } from '../../store/slices/searchSlice';
import { useTypedDispatch } from '../../hooks';
import { COLORS } from '../../constants';
import { searchStyles as customStyles } from '../Search';

const Search = () => {
  const dispatch = useTypedDispatch();

  const [searchValue, setSearchValue] = useState<string>('');

  const handleChangeSearch = (text: string) => {
    setSearchValue(text);

    dispatch(searchActions.setSearchValue(text));
  };

  const handleClearText = () => {
    setSearchValue('');

    dispatch(searchActions.resetState());
  };

  return (
    <Input
      name="search"
      placeholder="Search..."
      value={searchValue}
      onChangeText={handleChangeSearch}
      prefix={<SearchIcon name="search" size={16} color={COLORS.GREY} />}
      suffix={
        !!searchValue.length && (
          <CrossIcon
            name="remove"
            size={16}
            color="red"
            onPress={handleClearText}
          />
        )
      }
      customStyles={customStyles}
    />
  );
};

export default memo(Search);
