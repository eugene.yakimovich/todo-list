export const searchStyles = {
  inputWrapper: {
    $marginBottom: 10,
    $maxWidth: 362,
  },
  inputField: {
    $maxWidth: 290,
  },
};
