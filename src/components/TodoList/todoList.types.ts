import { TodoType } from '../../types';

export type RenderItem = {
  item: TodoType;
};

export type NavigationParams = {
  id: number;
  title: string;
};
