import React, { memo } from 'react';
import { FlatList, RefreshControl, TouchableOpacity } from 'react-native';

import { useTypedDispatch } from '../../hooks';
import { TodoItem } from '../../components';
import { HomeScreenProps } from '../../types';
import { Loader, NoRecords } from '../../shared';
import { RECORDS_LIMIT } from '../../constants';
import { todoActions } from '../../store/slices/todoSlice';
import { useTodoList } from '../../screens/HomeScreen/hooks';
import { RenderItem, NavigationParams } from '../TodoList';

const TodoList: React.FC<HomeScreenProps> = ({ navigation }) => {
  const dispatch = useTypedDispatch();

  const { todoList, isLoaded, isTodosLoading, refetch } = useTodoList({
    limit: RECORDS_LIMIT,
  });

  const handleNavigateToTodoScreen = ({ id, title }: NavigationParams) => {
    navigation.navigate('Todo', {
      id,
      title,
    });
  };

  const handleDeleteTodoItem = (id: number) => {
    dispatch(todoActions.deleteTodo(id));
  };

  if (!isLoaded) {
    return <Loader size="large" />;
  }

  if (!todoList.length) {
    return <NoRecords />;
  }

  return (
    <FlatList
      refreshControl={
        <RefreshControl refreshing={isTodosLoading} onRefresh={refetch} />
      }
      keyExtractor={(item) => item.id.toString()}
      data={todoList}
      renderItem={({
        item: { todo, completed, id },
      }: RenderItem): JSX.Element => (
        <TouchableOpacity
          onPress={() => handleNavigateToTodoScreen({ id, title: todo })}
        >
          <TodoItem
            todo={todo}
            completed={completed}
            id={id}
            onDelete={(id: number) => handleDeleteTodoItem(id)}
          />
        </TouchableOpacity>
      )}
    />
  );
};

export default memo(TodoList);
