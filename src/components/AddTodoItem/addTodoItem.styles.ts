import { styled } from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  gap: 10px;
`;

export const addTodoItemStyles = {
  inputWrapper: {
    $maxWidth: 253,
  },
  inputField: {
    $maxWidth: 205,
  },
};
