import React, { memo, useState } from 'react';
import CrossIcon from 'react-native-vector-icons/FontAwesome';

import { Button, Input } from '../../shared';
import { useTypedDispatch } from '../../hooks';
import { todoActions } from '../../store/slices/todoSlice';
import { Container, addTodoItemStyles as customStyles } from '../AddTodoItem';
import { TodoType } from '../../types';

const AddTodoItem: React.FC = () => {
  const dispatch = useTypedDispatch();

  const [value, onChangeValue] = useState<string>('');

  const handleAddTodo = () => {
    const id: number = Math.random();

    const payload: TodoType = {
      id,
      todo: value,
      userId: id + id,
      completed: false,
    };

    dispatch(todoActions.addTodo(payload));
    onChangeValue('');
  };

  const handleChangeText = (text: string) => {
    onChangeValue(text);
  };

  const handleClearText = () => {
    onChangeValue('');
  };

  return (
    <Container>
      <Input
        name="input"
        placeholder="Enter a value..."
        value={value}
        onChangeText={handleChangeText}
        customStyles={customStyles}
        suffix={
          !!value.length && (
            <CrossIcon
              name="remove"
              size={16}
              color="red"
              onPress={handleClearText}
            />
          )
        }
      />

      <Button onPress={handleAddTodo} disabled={!value.length}>
        Create
      </Button>
    </Container>
  );
};

export default memo(AddTodoItem);
