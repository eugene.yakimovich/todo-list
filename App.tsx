import { Provider } from 'react-redux';

import { Navigation } from './src/screens';
import { setupStore } from './src/store/store';

export default function App() {
  const store = setupStore();

  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}
